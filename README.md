# standard-libs

Library of standard electronics components used by AmadorUAVs Electromechanics.

## How to Import

Add this library as a submodule using `git submodule add https://gitlab.com/amadoruavs/electrical/standard-libs`.
Then add the files to the KiCad project using the standard library manager.

Make sure to select "Project Table" and not "Global Table" for libraries.

## An Explanation of Components

### Regulators

- BL9161-33BARN/HX6211A332MR: 3.3V LDO Regulator, standard for MCU power
- LDK120M18R: 5V LDO Regulator

### Switches

- TS_1088R: SPST Push-button Switch

### Connectors

- U254-051T-4BH83-F1S: Micro-USB Connector
- Terminal Block: Phoenix 5.08mm (Standard Library)

### Microcontrollers

- STM32G491CET6: 48-pin LQFP (standard package, custom schematic symbol), CAN-FD enabled, high compute power
- STM32G0B1KE: 32-pin LQFP (standard package, custom schematic symbol), CAN-FD enabled, medium compute power
- STM32G030F6P6: 20-TSSOP (standard package, custom symbol), no CAN, low power, good for small applications
- ESP32-WROOM-32(U/D) module: Wi-Fi enabled high-power/high-performance MCU (Standard Library)
- RP2040: 56-pin QFN (standard package, custom symbol), very high compute power, high supply, use where G030 not applicable and CAN not required
- ATSAMD21E17D-AU: 32-pin TQFP, low compute (Standard Library)
- ATSAMD21G17D-AU: 48-pin TQFP, low compute (Standard Library)

### Sensors

- ICM20789: Standard 7-axis IMU+baro, 24-QFN
- ICM20689: Standard 6-axis IMU, 24-QFN
- MMC5633NJL: Standard 3-axis Magnetometer, 4-WLP-0.86

### Logos

Well gee, open it up and take a look.


Direct questions to vwangsf@gmail.com or bbworld#6258.
